# MoMessages

This is a proposal that grew out of a discussion of how to handle messages that are not human conversation messages in Chatty.

**Problem**:
Messages like MMS, VVM & cell broadcast messages come across as SMS, but are not pleasant to see in a chat application and in the case of MMS, unreadable as they contain binary data. Put another way, multiple technologies piggyback on the SMS protocol.<br/>
Idea's proposed:
1) Ignoring the messages outright, which requires some coding to be added to Chatty to identify the message as something to ignore. Requires extra maintenance for things that are carrier dependent, like VVM.
2) Create a feedback method that would allow a technology handler, such as vvmd, to call back to Chatty and identify a certain message as something that can be hidden.
3) Manually hiding conversations identified by the user as system messages.

The author of this proposal identifies several other issues that he would like to improve.

**Herding**:
As each technology requires listening for and reading the message to be able to identify it as something to be handled, each technology must listen for the same signal and respond simultaneously.
i.e. Chatty, mmsd-tng, vvmd, with cell broadcast messages in the pipe and any other push notification handler's that might come about.
All of these daemons will see the signal, fetch, & parse the message at the same time, spiking system resources.

**Fragmented pipeline**:
MMS messages follow a vastly different flow than SMS messages do.
Luckily, MMSD was already created for handling such messages.
Unfortunately, that means Chatty must listen to two sources for messages, MM & mmsd-tng.
If something is wrong with the mmsd-tng pipeline, like it has crashed or is not installed, the user may not know that they are silently missing messages because Chatty does not know how to parse the initial SMS WAP notification, nor should it.
Further, each chat application wishing to implement MMS needs to handle two different methods of sending a message. Three, if they choose to support both MM and oFono.

**Security**:
With no good window for security auditing of incoming data. How does a handler like vvmd know that a message to set up the VM box is not from some H4X0R?
More precisely, there is no central location to implement 3GPP messaging standards that are beyond the scope of MM.

**Code duplication**:
There will always be some code that needs to be shared between modules (e.g. connecting to MoMessages or wherever a project chooses to monitor for messages).
However, mmsd-tng & vvmd both have plugin framework for connecting to the modem. Currently, only a MM plugin because nobody has written an oFono plugin.
Once somebody does though, that is now two projects implementing plugin framework and carrying support for two modem service daemons for identical tasks. Will they stay in sync with new developments and what is the maintenance burden?

Now, the stage is set for the open source world to duplicate all of this in its attempt to create a better chat app.

As the author was attempting to argue some of these points, a pioneer of implementing some of these technologies commented, "What I think you are arguing for is some sort of master arbitration app for Phosh in regards to SMS."

Yes! It should be light, and robust. A small core pluggable framework.<br/>
A stable API without unnecessary noise.<br/>
It should do the right thing, bundling human messages under one API and provide a security checkpoint for incoming data.<br/>
Always have a failsafe, so unhandled messages do not go unnoticed.<br/>
It should provide a framework that projects want to use.<br/>
Not some master daemon that implements every standard, but a filter. Just enough to get the data where it was meant for.<br/>

# Some high level guidelines

This project would facilitate the implementation of 3GPP message standards that are beyond the scope of MM.

This would largely be a rebrand of some, or all of mmsd-tng, whatever is deemed best.

Text SMS handling would need to be added and some registration code for modules.
Registration code could be modeled after MMSD's consumer API.
Regardless of whether the parser is up and running when the message is first seen,
there needs to be an assurance that a handler, if installed,
will be given a chance to parse the message before a new message API signal is generated.
The consumer API handles that by launching the dbus service before calling to it.
Modules may be loaded at startup, but it would not be necessary.

Plugable "modules" handle different WAP types or cell technologies e.g. MMS, VVM, or cell broadcast messages as examples.
These modules remain autonomous and need not be part of the momessages project.

Modem plugins fetch all SMS messages (i.e. data & text) and serve them to the parsing framework.
Data messages follow the current path, going to the WAP parser.
The text parsing framework can get hints from the MM message type and the 3GPP standard on whether they should be parsed,
but modules do the actual parsing.

Different API's: default is message, extendable through modules vvm, broadcast, etc.
MMS & SMS use the same outward facing API.

How would this help?

**Solution**:
Add an abstraction layer to be able to parse messages before they reach Chatty.
The user is not bothered by messages that are dealt with, but will notice if something is not set up right.
More importantly, proper message handling is maintained regardless of whether a handler is installed.
i.e. Ideally, a handler should delete the message from the SIM, however, if a handler is not installed, the messaging app needs to do it.
Chatty does not need to be taught how to parse anything.
No new back channels need to be created.

**Herding**:
MoMessages would do work that is being done by an uncapped number of clients.
Only momessages would need to watch for the signal, fetch, & parse the message. Some parsing may be skippable.
In the case of text SMS parsing, where parsing is done by the module, processing is already well under way with no extra steps.

**Fragmented pipeline**:
There would be one good pipeline for dealing with messages.
As soon as the notification is decoded, Chatty knows who it is from.
The message API would probably look a lot like MMSD's message API, but would also emit SMS messages.
MMS capability would be exposed as a property so that chat applications could know if attachments are supported.
Chatty would no longer need to listen to MM directly and would work on oFono systems assuming there was an oFono plugin for momessages.
With only one message API, applications would no longer need to read from multiple sources or have a split sending framework.
With the API looking like MMSD's, current MMSD code should work with little or no modification.

**Security**:
Maybe momessages would be incapable of helping here. But, if there is a way to prevent a vulnerability, having a central location to put up a firewall may become beneficial.
More than one project may benefit without the need to patch each individually.

**Code duplication**:
Daemon's do not need to carry code to support two different backends.
WAP handlers do not need to re-implement common parsing code.
All of the code that causes "Herding" and is the result of "Fragmented pipeline" could be eliminated across the ecosystem.


Random thoughts:

MoMessages refers to mobile-broadband-provider-info for help setting up APN settings.<br/>
Maybe reintroduce the consumer API from MMSD as a facility to signal any type of push notification if there is interest.<br/>
Optional disk storage through modules.<br/>
Otherwise, cache messages read from the modem in ram until an application fetches them or they are flushed.<br/>
Default yes option to delete the data SMS from the SIM after parsing.<br/>
This option should be tied to chatty's current behavior of deleting messages on the SIM.<br/>
Exposed option over dbus, chatty can set the option on user toggle.<br/>
Save choice in local settings to prevent race condition on startup.<br/>
Users could write their own parser to do fun or useful stuff with minimal work.
Like, a find my phone python script, which might get a location fix and send the coordinates to a predefined email or sound off with a highly audible ping tone.

Some example's

**Handled MMS**<br/>
Data SMS is fetched, parsed by WAP parser, found to be a WAP MMS.<br/>
Available WAP modules are probed, mmsd-tng is a handler.<br/>
mmsd-tng generates a new message signal through a MoMessages message API (state notification).<br/>
mmsd-tng sets up bearer handlers and begins download (state downloading).<br/>
message is downloaded (state downloaded).<br/>
Chatty has been watching the signal and is done displaying the message, requests delete. mmsd-tng deletes its local cache.<br/>

**Unhandled SMS**<br/>
Text SMS is fetched. It is parsed by modules until a handler is found.<br/>
No handler is served through the message API.<br/>
New message signal is generated (state downloaded).<br/>
Chatty requests the message, which is served via an in memory copy.<br/>
Chatty requests delete. The message is deleted from the SIM.<br/>

**Unhandled WAP message**<br/>
Data SMS is fetched, parsed by WAP parser, no handler is found.<br/>
The notification is stored on disk.<br/>
MoMessages generates a new text message signal via message API.<br/>
The message indicates the basic WAP type and the fact that it is unhandled. Maybe links to a database of WAP types.<br/>
Chatty requests, displays, & deletes the message.<br/>
The delete signal is converted to a message read flag and the notification stays on disk.<br/>
A new handler is installed and triggers momessages to reevaluate.<br/>
Maybe the new handler can handle some notifications.<br/>
No new error message signal is generated for still unhandled messages flagged as read.<br/>


Obviously, projects would still be able to talk with MM or oFono, but this author's hope is that this project would solve some fundamental protocol issues and developers would want to use it. At least for the software that is on his phone ;)
However, if there is not developer interest, then this proposal is the end of the line.


[Consumer API](https://git.kernel.org/pub/scm/network/ofono/mmsd.git/tree/doc/consumer.txt)
